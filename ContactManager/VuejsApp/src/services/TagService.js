import axios from 'axios'
import api from '@/utilities/api'

export default {
    addTag(tag) {
        return axios.put(`${api.url}/Tags/AddTag`, tag);
    },
    getTag(id) {
        return axios.get(`${api.url}/Tags/GetTag/${id}`);
    },
    getTags() {
        return axios.get(`${api.url}/Tags/GetTags`);
    },
    updateTag(tag) {
        return axios.post(`${api.url}/Tags/EditTag`, tag );
    },
    deleteTag(id) {
        return axios.delete(`${api.url}/Tags/DeleteTag/${id}`, );
    }



}