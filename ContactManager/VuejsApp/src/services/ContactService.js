import axios from 'axios'
import api from '@/utilities/api'

export default {
    addContact(contact) {
        return axios.put(`${api.url}/contacts/add`, contact);
    },
    getContact(id) {
        return axios.get(`${api.url}/contacts/get/${id}`);         
    },
    getContacts() {
        return axios.get(`${api.url}/contacts/GetAll`);
    },
    updateContact(contact) {
        return axios.post(`${api.url}/contacts/Edit`,contact);
    },
    getContactsByTag(tag) {
        return axios.post(`${api.url}/contacts/GetByTag?tag=${tag}`);
    },
    searchContacts(query) {
        return axios.post(`${api.url}/contacts/Search?query=${query}`);
    },
    deleteContact(id) {
        return axios.delete(`${api.url}/contacts/delete/${id}`);
    },
    deleteNumberForContact(contactId, numberId) {
        return axios.delete(`${api.url}/phonenumber/deletenumber/`, {
            data: {
                numberId: numberId, contactId: contactId
            }
        });
    },
    deleteEmailForContact(contactId, emailId) {
        return axios.delete(`${api.url}/email/deleteemail/`, {
            data:
            {
                emailId: emailId, contactId: contactId
            }
        });
}


}