import Vue from 'vue';
import App from '@/App.vue';
import router from '@/utilities/router'
import VeeValidate from 'vee-validate'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/css/style.css'
import VuejsDialog from "vuejs-dialog"
import 'vuejs-dialog/dist/vuejs-dialog.min.css'


Vue.use(VuejsDialog)
Vue.use(VeeValidate)
Vue.config.productionTip = true;

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
