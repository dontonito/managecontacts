﻿import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import TagContacts from '@/components/TagContacts'
import TagManagement from '@/components/TagManagement'
import ContactCreate from '@/components/ContactCreate'
import ContactEdit from '@/components/ContactEdit'
import NotFound from '@/components/NotFound'


Vue.use(Router)

export default new Router ({
    mode: 'history',
    root: 'path',
    routes: [
        {
            path: '/',
            name: 'Root',
            component: Home
        },
        {
            path: '/Home',
            name: 'Home',
            component: Home
        },
        {
            path: '/Tag/:tagName',
            name: 'TagContacts',
            component: TagContacts
        },
        {
            path: '/Manage/Tags',
            name: 'TagManagement',
            component: TagManagement
        },
        {
            path: '/contact/edit/:id',
            name: 'ContactEdit',
            component: ContactEdit
        },
        {
            path: '/contact/create',
            name: 'ContactCreate',
            component: ContactCreate
        },
        {
            path: '*',
            name: 'NotFound',
            component:NotFound
        }
    ]
})

