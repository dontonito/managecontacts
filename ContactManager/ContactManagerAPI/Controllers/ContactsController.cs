﻿using ContactManagerAPI.Models;
using ContactManagerAPI.Persistence;
using System.Web.Http;

namespace ContactManagerAPI.Controllers
{
    public class ContactsController : ApiController
    {
        private UnitOfWork _unitOfWork;

        public ContactsController()
        {
            _unitOfWork = new UnitOfWork();
        }

        public IHttpActionResult GetAll()
        {
            var contacts = _unitOfWork.Contacts.GetAllContacts();

            return Ok(contacts);
        }

        [HttpPost]
        public IHttpActionResult Search(string query)
        {

            var result = _unitOfWork.Contacts.Search(query);

            return Ok(result);
        }


        [HttpPost]
        public IHttpActionResult GetByTag(string tag)
        {
            var contacts = _unitOfWork.Contacts.GetAllForTag(tag);

            return Ok(contacts);
        }


        public IHttpActionResult Get(int id)
        {
            Contact contact = _unitOfWork.Contacts.GetContact(id);
            if (contact == null)
            {
                return NotFound();
            }

            return Ok(contact);
        }

        [HttpPost]
        public IHttpActionResult Edit(Contact contact)
        {
            var dbValue = _unitOfWork.Contacts.GetContact(contact.ContactId);

            if (dbValue == null)
                return NotFound();

            _unitOfWork.Contacts.Update(contact, dbValue);
            _unitOfWork.Complete();
            return Ok(contact);
        }

        [HttpPut]
        public IHttpActionResult Add(Contact contact)
        {
            _unitOfWork.Contacts.Add(contact);
            _unitOfWork.Complete();
            return Ok(contact);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Contact contact = _unitOfWork.Contacts.GetContact(id);
            if (contact == null)
            {
                return NotFound();
            }

            _unitOfWork.Contacts.Remove(contact);
            _unitOfWork.Complete();

            return Ok(contact);
        }



    }
}