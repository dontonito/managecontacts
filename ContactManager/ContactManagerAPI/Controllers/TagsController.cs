﻿using ContactManagerAPI.Models;
using ContactManagerAPI.Persistence;
using System.Collections.Generic;
using System.Web.Http;

namespace ContactManagerAPI.Controllers
{
    public class TagsController : ApiController
    {
        private UnitOfWork _unitOfWork;

        public TagsController()
        {
            _unitOfWork = new UnitOfWork();
        }


        public IEnumerable<Tag> GetTags()
        {
            return _unitOfWork.Tags.GetAllTags();
        }


        public IHttpActionResult GetTag(int id)
        {
            Tag tag = _unitOfWork.Tags.GetTag(id);
            if (tag == null)
            {
                return NotFound();
            }

            return Ok(tag);
        }

        [HttpPut]
        public IHttpActionResult AddTag(Tag tag)
        {

            _unitOfWork.Tags.Add(tag);
            _unitOfWork.Complete();

            return Ok(tag);
        }

        [HttpPost]
        public IHttpActionResult EditTag(Tag tag)
        {

            if (_unitOfWork.Tags.GetTag(tag.TagId) == null)
                return NotFound();

            _unitOfWork.Tags.Update(tag);
            _unitOfWork.Complete();

            return Ok(tag);
        }

        // DELETE: api/Tags/5
        [HttpDelete]
        public IHttpActionResult DeleteTag(int id)
        {
            Tag tag = _unitOfWork.Tags.GetTagWithContacts(id);
            if (tag == null)
                return NotFound();

            tag.ResetTag();

            _unitOfWork.Tags.Remove(tag);
            _unitOfWork.Complete();

            return Ok(tag);
        }

    }
}