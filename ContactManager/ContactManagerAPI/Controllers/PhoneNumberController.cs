﻿using ContactManagerAPI.ApiModels;
using ContactManagerAPI.Persistence;
using System.Web.Http;

namespace ContactManagerAPI.Controllers
{
    public class PhoneNumberController : ApiController
    {
        private UnitOfWork _unitOfWork;

        public PhoneNumberController()
        {
            _unitOfWork = new UnitOfWork();
        }

        [HttpDelete]
        public IHttpActionResult DeleteNumber([FromBody]NumberDeleteApiModel model)
        {
            var number = _unitOfWork.PhoneNumbers.GetPhoneNumber(model.NumberId);

            if (number == null)
                return NotFound();

            if (number.ContactId != model.ContactID)
                throw new System.Exception("ContactId for number does not match supplied contactid");

            _unitOfWork.PhoneNumbers.Remove(number);
            _unitOfWork.Complete();

            return Ok(number);
        }
    }
}
