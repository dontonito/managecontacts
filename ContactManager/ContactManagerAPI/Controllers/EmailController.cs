﻿using ContactManagerAPI.ApiModels;
using ContactManagerAPI.Persistence;
using System.Web.Http;

namespace ContactManagerAPI.Controllers
{
    public class EmailController : ApiController
    {
        private UnitOfWork _unitOfWork;

        public EmailController()
        {
            _unitOfWork = new UnitOfWork();
        }

        [HttpDelete]
        public IHttpActionResult DeleteEmail([FromBody]EmailDeleteAPIModel model)
        {
            var email = _unitOfWork.Emails.GetEmail(model.EmailId);

            if (email == null)
                return NotFound();

            if (email.ContactId != model.ContactId)
                throw new System.Exception("ContactId for email does not match supplied contactid");

            _unitOfWork.Emails.Remove(email);
            _unitOfWork.Complete();

            return Ok(email);
        }
    }
}
