namespace ContactManagerAPI.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                {
                    ContactId = c.Int(nullable: false, identity: true),
                    FirstName = c.String(nullable: false),
                    LastName = c.String(nullable: false),
                    TagId = c.Int(nullable: true),
                    Address = c.String(),
                    SkypeName = c.String(),
                })
                .PrimaryKey(t => t.ContactId)
                .ForeignKey("dbo.Tags", t => t.TagId)
                .Index(t => t.TagId);

            CreateTable(
                "dbo.EmailAddresses",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ContactId = c.Int(nullable: false),
                    Email = c.String(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);

            CreateTable(
                "dbo.PhoneNumbers",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ContactId = c.Int(nullable: false),
                    Number = c.String(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);

            CreateTable(
                "dbo.Tags",
                c => new
                {
                    TagId = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 75),
                })
                .PrimaryKey(t => t.TagId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Contacts", "TagId", "dbo.Tags");
            DropForeignKey("dbo.PhoneNumbers", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.EmailAddresses", "ContactId", "dbo.Contacts");
            DropIndex("dbo.PhoneNumbers", new[] { "ContactId" });
            DropIndex("dbo.EmailAddresses", new[] { "ContactId" });
            DropIndex("dbo.Contacts", new[] { "TagId" });
            DropTable("dbo.Tags");
            DropTable("dbo.PhoneNumbers");
            DropTable("dbo.EmailAddresses");
            DropTable("dbo.Contacts");
        }
    }
}
