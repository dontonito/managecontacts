﻿using ContactManagerAPI.Persistence;
using System;
using System.Web;
using System.Web.Http;

namespace ContactManagerAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.Initialize(true);
            }
            GlobalConfiguration.Configure(WebApiConfig.Register);

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

            var context = HttpContext.Current;
            var response = context.Response;

            // enable CORS
            response.AddHeader("Access-Control-Allow-Origin", "*");
            response.AddHeader("X-Frame-Options", "ALLOW-FROM *");

            if (context.Request.HttpMethod == "OPTIONS")
            {
                response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
                response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
                response.AddHeader("Access-Control-Max-Age", "1728000");
                response.End();
            }

        }
    }
}
