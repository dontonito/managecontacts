﻿namespace ContactManagerAPI.ApiModels
{
    public class EmailDeleteAPIModel
    {
        public int ContactId { get; set; }
        public int EmailId { get; set; }
    }
}