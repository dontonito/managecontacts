﻿namespace ContactManagerAPI.ApiModels
{
    public class NumberDeleteApiModel
    {
        public int ContactID { get; set; }
        public int NumberId { get; set; }
    }
}