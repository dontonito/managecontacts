﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ContactManagerAPI.Models
{
    public class Contact
    {
        [Key]
        public int ContactId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public int? TagId { get; set; }

        public string Address { get; set; }

        public string SkypeName { get; set; }

        public Tag Tag { get; set; }

        public ICollection<EmailAddress> EmailAddresses { get; set; }

        public ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
}