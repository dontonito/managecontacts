﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ContactManagerAPI.Models
{
    public class Tag
    {
        [Key]
        public int TagId { get; set; }

        [Required]
        [StringLength(75)]
        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<Contact> Contacts { get; set; }

        public void ResetTag()
        {
            Contacts.ToList().ForEach(u => u.TagId = null);
        }

    }
}