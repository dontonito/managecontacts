﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ContactManagerAPI.Models
{
    public class PhoneNumber
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int ContactId { get; set; }

        [Required]
        public string Number { get; set; }

        [JsonIgnore]
        public Contact Contact { get; set; }
    }
}