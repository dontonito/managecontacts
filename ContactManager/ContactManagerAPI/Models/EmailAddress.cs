﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ContactManagerAPI.Models
{
    public class EmailAddress
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int ContactId { get; set; }

        [Required]
        public string Email { get; set; }

        [JsonIgnore]
        public Contact Contact { get; set; }
    }
}