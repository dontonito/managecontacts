﻿using ContactManagerAPI.Models;
using System.Data.Entity;

namespace ContactManagerAPI.Persistence
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext()
        {
            Database.SetInitializer(new DatabaseInitializer());
        }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<EmailAddress> EmailAddresses { get; set; }

        public DbSet<PhoneNumber> PhoneNumbers { get; set; }

    }
}