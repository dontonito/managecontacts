﻿using ContactManagerAPI.Models;
using System.Linq;

namespace ContactManagerAPI.Persistence.Repository
{
    public class EmailRepository
    {

        private ApplicationDbContext _context;

        public EmailRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public EmailAddress GetEmail(int EmailID)
        {
            return _context.EmailAddresses.SingleOrDefault(u => u.Id == EmailID);
        }

        public void Remove(EmailAddress email)
        {
            _context.EmailAddresses.Remove(email);
        }
    }
}