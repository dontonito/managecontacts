﻿using ContactManagerAPI.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ContactManagerAPI.Persistence.Repository
{
    public class ContactRepository
    {
        private ApplicationDbContext _context;

        public ContactRepository(ApplicationDbContext context)
        {
            _context = context;
        }



        public IEnumerable<Contact> Search(string query)
        {
            return _context.Contacts.
                 Include(u => u.Tag)
                .Include(u => u.EmailAddresses)
                .Include(u => u.PhoneNumbers)
                .Where(u => u.Tag.Name.Contains(query) ||
                            u.FirstName.Contains(query) ||
                            u.LastName.Contains(query)
                            )
                .ToList();
        }

        public IEnumerable<Contact> GetAllForTag(string tag)
        {
            return _context.Contacts.
                 Include(u => u.Tag)
                .Include(u => u.EmailAddresses)
                .Include(u => u.PhoneNumbers)
                .Where(u => u.Tag.Name == tag)
                .ToList();
        }

        public IEnumerable<Contact> GetAllContacts()
        {
            return _context.Contacts.
                 Include(u => u.Tag)
                .Include(u => u.EmailAddresses)
                .Include(u => u.PhoneNumbers).ToList();
        }


        public Contact GetContact(int id)
        {
            return _context.Contacts
                .Include(u => u.Tag)
                .Include(u => u.EmailAddresses)
                .Include(u => u.PhoneNumbers)
                .SingleOrDefault(u => u.ContactId == id);
        }

        public void Add(Contact contact)
        {
            _context.Contacts.Add(contact);
        }

        public void Remove(Contact contact)
        {
            _context.Contacts.Remove(contact);
        }

        public void UpdateNavigationProperties(Contact contact, Contact dbValue)
        {

            contact.EmailAddresses.ToList().ForEach(u =>
            {
                if (u.Id == default(int))
                {
                    _context.EmailAddresses.Add(u);
                }
                else
                {
                    var dbEmail = dbValue.EmailAddresses.First(t => t.Id == u.Id);
                    _context.Entry(dbEmail).CurrentValues.SetValues(u);

                }
            }
            );

            contact.PhoneNumbers.ToList().ForEach(u =>
            {
                if (u.Id == default(int))
                {
                    _context.PhoneNumbers.Add(u);
                }
                else
                {
                    var dbNumber = dbValue.PhoneNumbers.First(t => t.Id == u.Id);
                    _context.Entry(dbNumber).CurrentValues.SetValues(u);

                }

            }
        );
        }

        public void Update(Contact contact, Contact dbValue)
        {
            UpdateNavigationProperties(contact, dbValue);
            _context.Entry(dbValue).CurrentValues.SetValues(contact);


        }
    }
}