﻿using ContactManagerAPI.Models;
using System.Linq;

namespace ContactManagerAPI.Persistence.Repository
{
    public class PhoneNumberRepository
    {
        private ApplicationDbContext _context;

        public PhoneNumberRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public PhoneNumber GetPhoneNumber(int numberId)
        {
            return _context.PhoneNumbers.SingleOrDefault(u => u.Id == numberId);
        }

        public void Remove(PhoneNumber number)
        {
            _context.PhoneNumbers.Remove(number);
        }
    }
}