﻿using ContactManagerAPI.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ContactManagerAPI.Persistence.Repository
{
    public class TagRepository
    {
        private ApplicationDbContext _context;

        public TagRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public Tag GetTag(int id)
        {
            return _context.Tags.Find(id);
        }


        public Tag GetTagWithContacts(int id)
        {
            return _context.Tags
                .Include(u => u.Contacts)
                .FirstOrDefault(u => u.TagId == id);

        }
        public IEnumerable<Tag> GetAllTags()
        {
            return _context.Tags.ToList();
        }

        public void Add(Tag tag)
        {
            _context.Tags.Add(tag);
        }

        public void Remove(Tag tag)
        {
            _context.Tags.Remove(tag);
        }


        public void Update(Tag tag)
        {
            var oldTag = _context.Tags.Find(tag.TagId);
            _context.Entry(oldTag).CurrentValues.SetValues(tag);
        }
    }
}