﻿using ContactManagerAPI.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace ContactManagerAPI.Persistence
{
    public class DatabaseInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            context.Tags.AddOrUpdate(x => x.TagId,
                new Tag { TagId = 1, Name = "Family" },
                new Tag { TagId = 2, Name = "Work" },
                new Tag { TagId = 3, Name = "Friend" },
                new Tag { TagId = 4, Name = "Highschool" },
                new Tag { TagId = 5, Name = "Music Legend" }
                );

            context.Contacts.AddOrUpdate(x => x.ContactId,
               new Contact
               {
                   FirstName = "John",
                   LastName = "Lennon",
                   Address = "7 Cavendish Avenue",
                   TagId = 5,
                   SkypeName = "lennon2",
                   EmailAddresses = new List<EmailAddress> {
                       new EmailAddress{ Email = "jlennon@liverpool.com" },
                       new EmailAddress{ Email = "jlennon@beatles.com" }
                   },
                   PhoneNumbers = new List<PhoneNumber> {
                       new PhoneNumber{ Number = "+44 20 7946 0235" },
                       new PhoneNumber{ Number = "+44 20 7946 0543" }
                   }
               },
                new Contact
                {
                    FirstName = "Bono",
                    LastName = "Vox",
                    TagId = 3,
                    Address = "10 Cedarwood Road",
                    SkypeName = "bonoU2",
                    EmailAddresses = new List<EmailAddress> {
                        new EmailAddress{ Email = "bono@u2.com" }
                    },
                    PhoneNumbers = new List<PhoneNumber> {
                        new PhoneNumber{ Number = "914-313-2363" },
                        new PhoneNumber{ Number = "+44 20 7946 0392" }
                    }
                },
                new Contact
                {
                    FirstName = "Teresa ",
                    LastName = "Martinez",
                    TagId = 1,
                    Address = "2330 Pallet Street",
                    SkypeName = "iiwaiw7ohC",
                    EmailAddresses = new List<EmailAddress> {
                        new EmailAddress{ Email = "TeresaKMartinez@jourrapide.com" }
                    },
                    PhoneNumbers = new List<PhoneNumber> {
                        new PhoneNumber{ Number = "+44 20 7946 0392" },
                        new PhoneNumber{ Number = "+48 20 231 0392" }
                    }
                },
                new Contact
                {
                    FirstName = "Joan",
                    LastName = "Acevedo",
                    TagId = 1,
                    Address = "12267 Andy Street",
                    SkypeName = "Enecildn",
                    EmailAddresses = new List<EmailAddress> {
                        new EmailAddress{ Email = "JoanSAcevedo@teleworm.us" }
                    },
                    PhoneNumbers = new List<PhoneNumber> {
                        new PhoneNumber{ Number = "605-387-7737" },
                        new PhoneNumber{ Number = "321-387-677" }
                    }
                }
            );
        }
    }
}