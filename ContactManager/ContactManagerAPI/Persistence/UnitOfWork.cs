﻿using ContactManagerAPI.Persistence.Repository;

namespace ContactManagerAPI.Persistence
{
    public class UnitOfWork
    {
        private ApplicationDbContext _context;
        public ContactRepository Contacts;
        public TagRepository Tags;
        public PhoneNumberRepository PhoneNumbers;
        public EmailRepository Emails;

        public UnitOfWork()
        {
            _context = new ApplicationDbContext();
            Contacts = new ContactRepository(_context);
            Tags = new TagRepository(_context);
            PhoneNumbers = new PhoneNumberRepository(_context);
            Emails = new EmailRepository(_context);
        }

        public void Complete()
        {
            _context.SaveChanges();
        }
    }
}